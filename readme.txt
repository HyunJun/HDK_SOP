HDK(Houdini Development toolKit) SOP 에 대한 코드입니다.

Visual Studio 2015(VC14) 템플릿도 함께 제공됩니다.

환경변수 SETUP 참고용

HOME
C:\..\HOME

H15_PATH
C:\Program Files\Side Effects Software\Houdini %H15_VERSION%

H15_VERSION
15.5.480

HOUDINI_DSO_PATH
%CUSTOM_DSO_PATH%;&

CUSTOM_DSO_PATH
C:%HOME%houdini15.5\dso
